# Machine Learning Based Classification of Magnetization Fields

Swapneel Amit Pathak<sup>1,2</sup>, Kurt Rahir<sup>3</sup>, Samuel J. R. Holt<sup>1,2</sup>, Martin Lang<sup>1,2,3</sup>, and Hans Fangohr<sup>1,2,3</sup>

<sup>1</sup> *Max Planck Institute for the Structure and Dynamics of Matter, Luruper Chaussee 149, 22761 Hamburg, Germany*  
<sup>2</sup> *University of Southampton, Southampton SO17 1BJ, United Kingdom*  
<sup>3</sup> *Center for Free-Electron Laser Science, Luruper Chaussee 149, 22761 Hamburg, Germany*  

| Description | Badge |
| --- | --- |
| Paper | Coming soon ... |
| Binder | [![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Ffangohr%2Fpaper-2023-machine-learning-based-classification-of-magnetization.git/HEAD?labpath=results.ipynb) |
| License | [![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) |
| DOI | [![DOI](badges/doi.svg)](https://doi.org/10.17617/3.KG33A1) |


## About

This repository provides simulation, data generation files and machine learning techniques used to complement
results from the paper: Machine learning based classification of vector field configurations. The `results.ipynb`
notebook contains the central clustering results of the paper and the methods used to obtain them. The `sims`
folder contains all the micromagnetic simulation data and the `data-generation-scripts` folder contains the
scripts used to generate the data on a high performance computer clusters.


## Binder

Jupyter notebooks hosted in this repository can be executed and modified in the
cloud via Binder. This does not require you to have anything installed and no
files will be created on your machine. To access Binder, click on the Binder
badge in the table above. For faster execution we recommend a local installation.


## License

Licensed under the BSD 3-Clause "New" or "Revised" License. For details, please
refer to the [LICENSE](LICENSE) file.

## Acknowlegdements

- EPSRC Programme Grant on [Skyrmionics](http://www.skyrmions.ac.uk) (EP/N032128/1)
